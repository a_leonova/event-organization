package com.backend.Session;

public class Session {
    private String userID;
    private long activity;

    public Session(String userID) {
        this.userID = userID;
        activity = System.currentTimeMillis();
    }

    public String getUserID() {
        return userID;
    }

    public long getActivity() {
        return activity;
    }

    public void setActivity(long activity) {
        this.activity = activity;
    }
}
