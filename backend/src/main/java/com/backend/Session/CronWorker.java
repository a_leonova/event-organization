//package com.backend.Session;
//
//import com.backend.Repositories.SessionRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//@Component("CronWorker")
//public class CronWorker {
//    @Autowired
//    private SessionRepository sessionRepository;
//
//    @Scheduled(fixedDelay = 1800000000)
//    public void deleteOldSessions() {
//        System.out.println("\nHELLO WORLD!\n");
//        sessionRepository.checkSessions();
//    }
//}
