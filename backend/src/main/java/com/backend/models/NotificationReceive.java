package com.backend.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class NotificationReceive {

    private String notificationID;
    private String type;
    private String senderID;
    private String eventID;
    private String serviceID;
    private String oldServiceID = "";

    public NotificationReceive(){}

    public NotificationReceive(String type, String senderID, String eventID, String serviceID){
        this.type = type;
        this.senderID = senderID;
        this.eventID = eventID;
        this.serviceID = serviceID;
    }

    public NotificationReceive(String type, String senderID, String eventID, String serviceID, String oldServiceID){
        this.type = type;
        this.senderID = senderID;
        this.eventID = eventID;
        this.serviceID = serviceID;
        this.oldServiceID = oldServiceID;
    }

    public String getOldServiceID() {
        return oldServiceID;
    }

    public void setOldServiceID(String oldServiceID) {
        this.oldServiceID = oldServiceID;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

}
