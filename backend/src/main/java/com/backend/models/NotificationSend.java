package com.backend.models;

public class NotificationSend {
    private String notificationID;
    private String type;
    private UserInfo sender;
    private Event event;
    private Service service;
    private Service oldService;

    public Service getOldService() {
        return oldService;
    }

    public void setOldService(Service oldServiceID) {
        this.oldService = oldServiceID;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UserInfo getSender() {
        return sender;
    }

    public void setSender(UserInfo sender) {
        this.sender = sender;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
