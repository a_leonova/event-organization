package com.backend.models;

public class Service {
    private String serviceID;
    private String profession;
    private TimePeriod period;
    private boolean isBusy = false;


    public Service(){    }
    public Service(TimePeriod period, String profession){
        this.period = period;
        this.profession = profession;
    }

    public boolean equals(Service other) {
        return other == this ||
                profession.equals(other.profession) &&
                        period.equals(other.period) &&
                        isBusy == other.isBusy;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public void setBusy(boolean busy) {
        isBusy = busy;
    }

    public TimePeriod getPeriod() {
        return period;
    }

    public void setPeriod(TimePeriod period) {
        this.period = period;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

}
