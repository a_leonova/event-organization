package com.backend.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Event {
    private String eventID;
    private String eventName;
    private String orgId;
    private UserInfo organizer;
    private TimePeriod period;
    private ArrayList<String> participantsId = new ArrayList<>();
    private ArrayList<Service> requiredProfessions;
    private ArrayList<UserInfo> participantsInfo = new ArrayList<>();
    private String info;

    private HashMap<String, String> performers = new HashMap<>();

    public Event(String orgId, String eventName, TimePeriod period, ArrayList<Service> requiredProfessions) {
        this.orgId = orgId;
        this.eventName = eventName;
        this.period = period;
        this.requiredProfessions = requiredProfessions;
    }
    public Event(){
    }

    public String findAnyUsersService(String userID){
        for(Map.Entry<String, String> entry : performers.entrySet()){
            if (entry.getValue().equals(userID)){
                return entry.getKey();
            }
        }
        return null;
    }

    public void addParticipant(String serviceID, UserInfo participant){
        participantsInfo.add(participant);
        participantsId.add(participant.getUserID());
        performers.put(serviceID, participant.getUserID());
    }

    public void deleteParticipant(String serviceID, UserInfo participant){
        participantsInfo.remove(participant);
        participantsId.remove(participant.getUserID());
        performers.remove(serviceID, participant.getUserID());
    }

    public void addService(Service newService, UserInfo user){

        if(newService.isBusy()){
            participantsInfo.add(user);
            participantsId.add(user.getUserID());
            performers.put(newService.getServiceID(), user.getUserID());
        }
        requiredProfessions.add(newService);

    }

    public void deleteService(Service service, UserInfo user){
        if (service.isBusy()){
            if(user == null){
                throw new IllegalArgumentException("Service busy, but user is NULL");
            }
            participantsInfo.remove(user);
            participantsId.remove(user.getUserID());
            performers.remove(service.getServiceID());
        }
        requiredProfessions.remove(service);
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


    public HashMap<String, String> getPerformers() {
        return performers;
    }

    public void setPerformers(HashMap<String, String> performers) {
        this.performers = performers;
    }

    public ArrayList<String> getParticipantsId() {
        return participantsId;
    }

    public void setParticipantsId(ArrayList<String> participantsId) {
        this.participantsId = participantsId;
    }

    public TimePeriod getPeriod() {
        return period;
    }

    public void setPeriod(TimePeriod period) {
        this.period = period;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public UserInfo getOrganizer() {
        return organizer;
    }

    public void setOrganizer(UserInfo organizer) {
        this.organizer = organizer;
    }

    public ArrayList<Service> getRequiredProfessions() {
        return requiredProfessions;
    }

    public void setRequiredProfessions(ArrayList<Service> requiredProfessions) {
        this.requiredProfessions = requiredProfessions;
    }

    public ArrayList<UserInfo> getParticipantsInfo() {
        return participantsInfo;
    }

    public void setParticipantsInfo(ArrayList<UserInfo> participantsInfo) {
        this.participantsInfo = participantsInfo;
    }

}
