package com.backend.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimePeriod {

    private String start;
    private String end;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public TimePeriod(){}

    public TimePeriod(String start, String end){
        this.start = start;
        this.end = end;
    }

    public int compareStart(String otherStart) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date thisDateStart = df.parse(start);
        Date otherDateStart = df.parse(otherStart);
        return thisDateStart.compareTo(otherDateStart);
    }

    public int compareEnd(String otherEnd) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date thisDateEnd = df.parse(end);
        Date otherDateEnd = df.parse(otherEnd);
        return thisDateEnd.compareTo(otherDateEnd);
    }

    public Boolean isIntersecting(TimePeriod other) {
        long thisStartDate = toNumber(start);
        long thisEndDate = toNumber(end);
        long otherStartDate = toNumber(other.getStart());
        long otherEndDate = toNumber(other.getEnd());

        if (thisEndDate == -1 || thisStartDate == -1 || otherStartDate == -1 || otherEndDate == -1) {
            System.out.println("PARSE DATES EXCEPTION");
        }

        return !(thisStartDate < otherStartDate && thisEndDate < otherStartDate ||
                otherStartDate < thisStartDate && otherEndDate < thisStartDate);

    }

    public Boolean equals(TimePeriod other){
        return this == other || start.equals(other.start) && end.equals(other.end);
    }

    private long toNumber(String time) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date;
        try {
            date = df.parse(time);
        }
        catch (ParseException e) {
            return -1;
        }

        return date.getTime();
    }

    private String toString(long time){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        return sdf.format(new Date(time));
    }

}
