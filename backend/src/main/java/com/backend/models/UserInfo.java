package com.backend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class UserInfo {

    private String userID;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String login;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private String firstName;
    private String secondName;
    private String profession;

    private String phone;
    private String email;

    @JsonIgnore
    private ArrayList<String> notificationsID = new ArrayList<>();
    private ArrayList<TimePeriod> busyDates = new ArrayList<>();
    @JsonIgnore
    private ConcurrentHashMap<String, TimePeriod> job = new ConcurrentHashMap<>();


    public ConcurrentHashMap<String, TimePeriod> getJob() {
        return job;
    }

    public void setJob(ConcurrentHashMap<String, TimePeriod> job) {
        this.job = job;
    }

    public UserInfo(){}
    public UserInfo(String login, String password, String firstName, String secondName, String profession,
                    String phone, String email){

        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.secondName = secondName;
        this.profession = profession;
        this.phone = phone;
        this.email = email;

    }

    public boolean isBusy(TimePeriod date) {
        for (TimePeriod time : busyDates) {
            if (date.isIntersecting(time)) {
                return true;
            }
        }

        return false;
    }

    public void addBusyDate(String serviceID, TimePeriod date) {
        busyDates.add(date);
        job.put(serviceID, date);
    }
    public void deleteBusyDate(String serviceID, TimePeriod date){
        busyDates.remove(date);
        job.remove(serviceID, date);
    }


    public ArrayList<String> getNotificationsID() {
        return notificationsID;
    }

    public void setNotificationsID(ArrayList<String> notificationsID) {
        this.notificationsID = notificationsID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public ArrayList<TimePeriod> getBusyDates() {
        return busyDates;
    }

    public void setBusyDates(ArrayList<TimePeriod> busyDates) {
        this.busyDates = busyDates;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addNotification(String notificationID){
        notificationsID.add(notificationID);
    }
    public void deleteNotification(String notificationID){
        notificationsID.remove(notificationID);
    }

    private TimePeriod findEqualTime(TimePeriod other){
        for(TimePeriod time : busyDates){
            if(time.equals(other)) {
                return time;
            }
        }
        return null;
    }
}
