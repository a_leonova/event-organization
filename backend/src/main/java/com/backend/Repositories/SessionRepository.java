package com.backend.Repositories;

import com.backend.Session.Session;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Repository
public class SessionRepository {
    private final static long SESSION_LIFETIME = 1800000000;
    private ConcurrentMap<String, Session> sessions = new ConcurrentHashMap<>();

    public void addSession(String cookie, String userID) {
        Session session = new Session(userID);
        sessions.put(cookie, session);
    }

    public void checkSessions() {
        for (Session session : sessions.values()) {
            if (System.currentTimeMillis() - session.getActivity() >= SESSION_LIFETIME) {
                sessions.values().remove(session);
            }
        }
    }

    public void updateSession(String cookie) {
        sessions.get(cookie).setActivity(System.currentTimeMillis());
    }

    public boolean isThereSession(String cookie) {
        return cookie != null && sessions.containsKey(cookie);
    }

    public boolean isAccessAllowed(String userID, HttpServletRequest httpServletRequest) {
        String userIDBySession = getUserID((httpServletRequest.getHeader("Cookie")));

        return userIDBySession.equals(userID);
    }

    public void deleteSession(String userID) {
        for (Session session : sessions.values()) {
            if (session.getUserID().equals(userID)) {
                sessions.values().remove(session);
            }
        }
    }

    public String getUserID(String cookie) {
        return sessions.get(cookie).getUserID();
    }
}
