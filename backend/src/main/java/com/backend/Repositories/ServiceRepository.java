package com.backend.Repositories;

import com.backend.models.NotificationReceive;
import com.backend.models.Service;
import com.backend.models.TimePeriod;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class ServiceRepository {
    private ConcurrentMap<String, Service> services = new ConcurrentHashMap<>();
    private final AtomicLong sequence = new AtomicLong(1);

    public Service findService(String id){
        return services.get(id);
    }

    public void addService(Service service) {
        String id = String.valueOf(sequence.getAndIncrement());
        service.setServiceID(id);
        services.put(id, service);
    }

    public void deleteService(String serviceID) {
        services.remove(serviceID);
    }

    public ServiceRepository(){
        create();
    }

    private void create(){
        Service service = new Service();
        service.setProfession("Driver");
        TimePeriod period = new TimePeriod();
        period.setStart("2018-07-07 10:24:08");
        period.setEnd("2018-07-08 10:24:08");
        service.setPeriod(period);
        addService(service);
    }
}
