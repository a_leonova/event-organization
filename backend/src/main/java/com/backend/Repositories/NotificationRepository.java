package com.backend.Repositories;

import com.backend.models.NotificationReceive;
import com.backend.models.NotificationSend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class NotificationRepository {

    @Autowired EventRepository events;
    @Autowired UserRepository users;
    @Autowired ServiceRepository serviceRepository;

    private ConcurrentMap<String, NotificationReceive> notifications = new ConcurrentHashMap<>();
    private final AtomicLong sequence = new AtomicLong(1);


    public NotificationRepository(){
        create();
    }

    public NotificationReceive findNotification(String id){
        return notifications.get(id);
    }

    public void addNotification(NotificationReceive notification) {

        String id = String.valueOf(sequence.getAndIncrement());
        notification.setNotificationID(id);
        notifications.put(id, notification);
    }


    public void deleteNotification(String notificationId) {
        notifications.remove(notificationId);
    }

    public NotificationSend receiveToSend(NotificationReceive received){
        NotificationSend send = new NotificationSend();

        send.setNotificationID(received.getNotificationID());
        send.setEvent(events.findEvent(received.getEventID()));
        send.setSender(users.findUserByID(received.getSenderID()));
        send.setService(serviceRepository.findService(received.getServiceID()));
        send.setType(received.getType());

        return send;
    }

    private void create(){
        //event4
        NotificationReceive notification = new NotificationReceive("JOB_SUGGESTION", "4",
                "5", "10" );
        addNotification(notification);


    }

}
