package com.backend.Repositories;

import com.backend.models.TimePeriod;
import com.backend.models.UserInfo;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class UserRepository {
    private final AtomicLong sequence = new AtomicLong(1);
    private Map<String, UserInfo> users = new HashMap<>();
    private PasswordEncoder encoder = new BCryptPasswordEncoder();

    public UserRepository() {
        registerUsers();
    }

    public UserInfo findUserByLogin(String login){

        for (Map.Entry<String, UserInfo> entry : users.entrySet()) {
            if (entry.getValue().getLogin().equals(login)) {
                return entry.getValue();
            }
        }

        return null;
    }

    public UserInfo findUserByID(String id){
        return users.get(id);
    }

    public void addUser(UserInfo newer){
        String id = String.valueOf(sequence.getAndIncrement());
        newer.setUserID(id);
        users.put(id, newer);
    }

    public ArrayList<UserInfo> getAllUsers() {

        ArrayList<UserInfo> userInfos = new ArrayList<>();

        Collection collection = users.values();

        for (Object aCollection : collection) {
            userInfos.add((UserInfo) aCollection);
        }


        return userInfos;
    }

    private void registerUsers() {
        UserInfo tanya = new UserInfo("tanya", encoder.encode("tanya"),
                "Таня","Дементьева","Ведущий",
                "89537981871", "tanya@gmail.com" );
        addUser(tanya);

        UserInfo yulia = new UserInfo("yulia", encoder.encode("yulia"),
                "Юлия","Красник","Флорист",
                "89534581871", "yulia@gmail.com" );
        addUser(yulia);

        UserInfo katya = new UserInfo("katya", encoder.encode("katya"),
                "Катя","Снегирева","Бармен",
                "89783810114", "katya@gmail.com" );
        addUser(katya);

        UserInfo nastya = new UserInfo("nastya", encoder.encode("nastya"),
                "Настя","Леонова","Фотограф",
                "89963802468", "nastya@gmail.com" );
        addUser(nastya);

        UserInfo dima = new UserInfo("dima", encoder.encode("dima"),
                "Дима","Селезнев","Официант",
                "89137849222", "dima@gmail.com" );
        addUser(dima);

        UserInfo vadim = new UserInfo("vadim", encoder.encode("vadim"),
                "Вадим","Коршунов","Диджей",
                "891345780014", "vadim@gmail.com" );
        addUser(vadim);

        UserInfo aia = new UserInfo("aia", encoder.encode("aia"),
                "Айя","Галиева","Аниматор",
                "89996414321", "aia@gmail.com" );
        addUser(aia);

        UserInfo anya = new UserInfo("anya", encoder.encode("anya"),
                "Аня","Кутергина","Музыкант",
                "89961258483", "anya@gmail.com" );
        addUser(anya);

        UserInfo sergey = new UserInfo("sergey", encoder.encode("sergey"),
                "Сергей","Журавлев","Повар",
                "89147855302", "sergey@gmail.com" );
        addUser(sergey);

        UserInfo daniil = new UserInfo("daniil", encoder.encode("daniil"),
                "Даниил","Данилов","Водитель",
                "89991144586", "daniil@gmail.com" );
        addUser(daniil);

    }
}
