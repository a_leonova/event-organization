package com.backend.Repositories;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class ProfessionsRepository {
    private List<String> professions;

    public ProfessionsRepository() {
        initialize();
    }

    public boolean isExistProfession(String profession) {
        for (String job : professions) {
            if (job.equals(profession)) {
                return true;
            }
        }

        return false;
    }

    public List<String> getProfessions() {
        return professions;
    }

    public void addProfession(String profession) {
        professions.add(profession);
    }

    private void initialize() {
        professions = Arrays.asList("Аниматор", "Бармен","Ведущий" ,"Видеограф", "Визажист", "Водитель",
                "Декоратор", "Диджей", "Звукорежиссер", "Кондитер", "Музыкант", "Официант", "Парикмахер",
                "Певец", "Пекарь", "Переводчик", "Повар", "Режиссер", "Стилист", "Фаерщик", "Флорист",
                "Фотограф", "Хореограф", "Художник", "Швея", "Ювелир");

    }
}
