package com.backend.Repositories;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class TokenRepository {
    private Map<String, String> tokens = new HashMap<>();
    private final String KEY = "AAAAI2zSNfY:APA91bERTtYgqCf_073M-o7_yaRpr6lrhYnyUhgxdRH6iMkA-SlXEmxn4ym5sdIf0SyJnXg4b0mY1spVltX9B8c_T2T1gY_IA0uV8Bq7x90ZEU29lHzKk1I0uYfT4D0cxGrpzpmQQR27oV6ttrQaKFBDIt833_px6Q";

    public void setToken(String userID, String token) {
        tokens.put(userID, token);
    }

    public String getToken(String userID) {
        return tokens.get(userID);
    }

    public void deleteToken(String userID) {
        tokens.remove(userID);
    }

    public String getKEY() {
        return KEY;
    }
}
