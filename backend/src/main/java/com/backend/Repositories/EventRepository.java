package com.backend.Repositories;

import com.backend.models.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class EventRepository {
    private final AtomicLong sequence = new AtomicLong(1);
    private Map<String, Event> events = new HashMap<>();

    @Autowired UserRepository users;
    @Autowired ServiceRepository serviceRepository;

    public void addNewEvent(Event event) {
        String id;
        if(event.getEventID() == null){
            id = String.valueOf(sequence.incrementAndGet());
            event.setEventID(id);
        }
        else{
            id = event.getEventID();
        }
        events.put(id, event);
    }

    public Event findEvent(String eventId){
        return events.get(eventId);
    }

    public ArrayList<Event> findOrgEvents(String userId){
        ArrayList<Event> needEvents = new ArrayList<>();

        for(Map.Entry<String, Event> entry: events.entrySet()){
            if(entry.getValue().getOrgId().equals(userId)){
                needEvents.add(entry.getValue());
            }
        }

        return needEvents;
    }

    public ArrayList<Event> findPartEvents(String userId){
        ArrayList<Event> needEvents = new ArrayList<>();

        for(Map.Entry<String, Event> entry: events.entrySet()){
            if (entry.getValue().getParticipantsId().contains(userId)) {
                needEvents.add(entry.getValue());
            }
        }

        return needEvents;
    }

    public void deleteEvent(String eventId) {
        events.remove(eventId);
    }

}
