package com.backend.API;


import com.backend.Repositories.EventRepository;
import com.backend.Repositories.ServiceRepository;
import com.backend.Repositories.SessionRepository;
import com.backend.Repositories.UserRepository;
import com.backend.models.Event;
import com.backend.models.Service;
import com.backend.models.TimePeriod;
import com.backend.models.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class EventController {

    @Autowired
    private EventRepository events;
    @Autowired
    private UserRepository users;
    @Autowired
    private ServiceRepository serviceRepository;
    @Autowired
    private NotificationManager notificationManager;
    @Autowired
    private SessionRepository sessionRepository;


    @GetMapping("api/users/{id}/events") public @ResponseBody Response getEvents(@PathVariable String id,
                                                                                 @RequestParam String role,
                                                                                 HttpServletRequest httpServletRequest){
        Response response = new Response();

//        if (!sessionRepository.isAccessAllowed(id, httpServletRequest)) {
//            return new Response("NOT_ALLOWED");
//        }

        if(users.findUserByID(id) == null){
            response.setData("USER_NOT_FOUND");
            return response;
        }

        switch (role) {
            case "ORG":
                response.setStatus("OK");
                response.setData(events.findOrgEvents(id));
                break;
            case "PART":
                response.setStatus("OK");
                response.setData(events.findPartEvents(id));
                break;
            default:
                response.setStatus("INCORRECT_REQUEST");
                break;
        }

        return response;
    }

    @GetMapping("api/events/{eventID}")
    public @ResponseBody Response getFullEventInfo(@PathVariable String eventID, HttpServletRequest httpServletRequest){
        Response response = new Response();
        Event event = events.findEvent(eventID);
//        String orgID = events.findEvent(eventID).getOrgId();
//        boolean isParticipantID = false;

        if(event == null){
            return new Response("EVENT_NOT_FOUND");
        }

//        for (UserInfo user : event.getParticipantsInfo()) {
//            if (sessionRepository.isAccessAllowed(user.getUserID(), httpServletRequest)) {
//                isParticipantID = true;
//            }
//        }
//
//        if (!sessionRepository.isAccessAllowed(orgID, httpServletRequest) || !isParticipantID) {
//            return new Response("NOT_ALLOWED");
//        }

        response.setStatus("OK");
        response.setData(event);

        return response;
    }

    @PostMapping("api/events") public @ResponseBody Response createEvent(@RequestBody Event newEvent, HttpServletRequest httpServletRequest){
        Response response = new Response();
        UserInfo user = users.findUserByID(newEvent.getOrgId());

//        if (!sessionRepository.isAccessAllowed(newEvent.getOrgId(), httpServletRequest)) {
//            response.setStatus("NOT_ALLOWED");
//            return response;
//        }

        if(user == null){
            response.setStatus("USER_NOT_FOUND");
            return response;
        }

        if(isBadTimePeriod(newEvent.getPeriod())){
            response.setStatus("BAD_TIME");
            return response;
        }

        if(user.isBusy(newEvent.getPeriod())){
            response.setStatus("BUSY_DATE");
            return response;
        }

        try {
            if(isBadEventTimeWithService(newEvent)){
                response.setStatus("BAD_SERVICE_TIME");
                return response;
            }
        } catch (ParseException e) {
            response.setStatus("BAD_TIME");
            return response;
        }

        for (Service service : newEvent.getRequiredProfessions()){
            serviceRepository.addService(service);
        }

        newEvent.setOrganizer(user);
        Service service = new Service(newEvent.getPeriod(), "ORGANIZER");
        serviceRepository.addService(service);
        user.addBusyDate(service.getServiceID(), newEvent.getPeriod());
        events.addNewEvent(newEvent);

        response.setStatus("OK");
        response.setData(newEvent.getEventID());

        return response;
    }

    @PostMapping("api/events/change") public @ResponseBody Response changeEvent(@RequestBody Event newEvent) {
        if (users.findUserByID(newEvent.getOrgId()) == null){
            return new Response("USER_NOT_FOUND");
        }
        if(events.findEvent(newEvent.getEventID()) == null){
            return new Response("EVENT_NOT_FOUND");
        }
//        if(!users.findUserByID(newEvent.getOrgId()).getUserID().equals(
//                events.findEvent(newEvent.getEventID()).getOrgId())){
//            return new Response("BAD_NEW_USER");
//        }
        Event oldEvent = events.findEvent(newEvent.getEventID());

        if(!oldEvent.getEventName().equals(newEvent.getEventName())){
            notificationManager.changeNameNotification(oldEvent);
            oldEvent.setEventName(newEvent.getEventName());
        }

        if(!oldEvent.getPeriod().equals(newEvent.getPeriod())){

            try {
                if(isBadTimePeriod(newEvent.getPeriod()) || isBadEventTimeWithService(newEvent)){
                    return new Response("BAD_TIME");
                }
            } catch (ParseException e) {
                return new Response("BAD_TIME");
            }
            oldEvent.setPeriod(newEvent.getPeriod());
        }

        for(Service newService : newEvent.getRequiredProfessions()){
            //new profession was added
            if(newService.getServiceID() == null){
                serviceRepository.addService(newService);
                oldEvent.addService(newService, null);
                continue;
            }

            //old profession was changed
            //if it wasn't busy - just change period
            //else: notify participant about newService
            // (newService will be put into serviceRepo even with new serviceID), so it's really new Service
            //but also there will be oldNotification date. so if notification.type == "CHANGED_TIME" oldService
            //is not null (else it's null)

            if(!newService.equals(serviceRepository.findService(newService.getServiceID()))){
                if (!newService.isBusy()){
                    serviceRepository.findService(newService.getServiceID()).setPeriod(newService.getPeriod());
                    continue;
                }

                String oldServiceID = newService.getServiceID();
                serviceRepository.addService(newService);
                UserInfo worker = users.findUserByID(oldEvent.getPerformers().get(oldServiceID));
                oldEvent.deleteService(serviceRepository.findService(oldServiceID),
                        worker);
                oldEvent.addService(newService, worker); //job is BOOKED

                notificationManager.changedTimeNotification(newService.getServiceID(), oldServiceID, oldEvent.getOrgId(),
                        oldEvent.getEventID(), worker.getUserID());
            }
        }

        oldEvent.setInfo(newEvent.getInfo());
        return new Response("OK", oldEvent);
    }

    @DeleteMapping("api/events/{id}") public  @ResponseBody Response deleteEvent(@PathVariable String id, HttpServletRequest httpServletRequest) {
        Response response = new Response();

//        if (!sessionRepository.isAccessAllowed(id, httpServletRequest)) {
//            return new Response("NOT_ALLOWED");
//        }

        events.deleteEvent(id);
        response.setStatus("OK");

        return response;
    }



    private boolean isBadTimePeriod(TimePeriod period) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        df.setLenient(false);
        Date dateStart;
        Date dateEnd;
        try {
            dateStart = df.parse(period.getStart());
            dateEnd = df.parse(period.getEnd());
        } catch (ParseException e) {
            return true;
        }

        return dateStart.compareTo(dateEnd) >= 0;
    }
    
    private boolean isBadEventTimeWithService(Event newEvent) throws ParseException {

        for(Service service : newEvent.getRequiredProfessions()){
            if(service.getPeriod().compareStart(newEvent.getPeriod().getStart()) < 0 ||
                    service.getPeriod().compareStart(newEvent.getPeriod().getEnd()) > 0 ||
                    isBadTimePeriod(service.getPeriod())){
                return true;
            }
        }

        return false;
    }

}
