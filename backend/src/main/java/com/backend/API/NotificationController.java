package com.backend.API;

import com.backend.Repositories.*;
import com.backend.models.NotificationReceive;
import com.backend.models.NotificationSend;
import com.backend.models.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.ArrayList;


@RestController
@CrossOrigin
public class NotificationController {

    @Autowired
    NotificationRepository notificationRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    EventRepository eventRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired NotificationManager notificationManager = new NotificationManager();

    @GetMapping("/api/users/{id}/notifications")
    public @ResponseBody Response getNotifications(@PathVariable String id,
                                                   @RequestParam int limit,
                                                   @RequestParam int offset){
        Response response = new Response();

        if(limit < 1 || offset < 0){
            response.setStatus("BAD_PARAM");
            return response;
        }

        UserInfo user = userRepository.findUserByID(id);
        if(user == null){
            response.setStatus("USER_NOT_FOUND");
            return response;
        }
        ArrayList<NotificationSend> notificationArrayList = notificationManager.getNotifications(offset,
                offset + limit, user);

        if (notificationArrayList.size() == limit){
            response.setStatus("OK");
        }
        else{
            response.setStatus("END");
        }
        response.setData(notificationArrayList);
        return response;
    }

    @PostMapping("api/notifications/sendJob")
    public @ResponseBody Response sendJobNotification(@RequestBody NotificationReceive notificationReceive){

        Response response = new Response();

        if(userRepository.findUserByID(notificationReceive.getSenderID()) == null){
            response.setStatus("USER_NOT_FOUND");
            return response;
        }
        if(serviceRepository.findService(notificationReceive.getServiceID()) == null){
            response.setStatus("SERVICE_NOT_FOUND");
            return response;
        }

        notificationReceive.setType("JOB_SUGGESTION");

        notificationRepository.addNotification(notificationReceive);


        notificationManager.sendJobNotification(notificationReceive,
                serviceRepository.findService(notificationReceive.getServiceID()));

        response.setStatus("OK");
        return response;
    }

    @PostMapping("/api/notifications/{notificationID}")
    public @ResponseBody Response replyToNotification(@PathVariable String notificationID,
                                                      @RequestParam String invitationStatus,
                                                      @RequestParam String userID) {

        Response response = new Response();
        NotificationReceive notification = notificationRepository.findNotification(notificationID);
        UserInfo user = userRepository.findUserByID(userID);

        if(notification == null){
            response.setStatus("NOTIFICATION_NOT_FOUND");
            return response;
        }

        if(user == null){
            response.setStatus("USER_NOT_FOUND");
            return response;
        }

        switch (invitationStatus){
            case "ACCEPTED":
                if(serviceRepository.findService(notification.getServiceID()).isBusy()){
                    response.setStatus("JOB_BOOKED");
                    user.deleteNotification(notificationID);
                    return response;
                }
                if (user.isBusy(serviceRepository.findService(notification.getServiceID()).getPeriod())){
                    response.setStatus("DATE_BUSY");
                    user.deleteNotification(notificationID);
                    return response;
                }

                notificationManager.sendJobAcceptedNotification(user, notification);
                response.setStatus("OK");
                break;
            case "REJECTED":
                user.deleteNotification(notificationID);
                response.setStatus("OK");
                break;
            case "ACCEPTED_TIME":
                if (user.isBusy(serviceRepository.findService(notification.getServiceID()).getPeriod())){
                    response.setStatus("DATE_BUSY");
                    notificationManager.rejectTimeNotification(user, notification);
                    return response;
                }
                notificationManager.acceptTimeNotification(user, notification);
                response.setStatus("OK");
            case "REJECTED_TIME":
                notificationManager.rejectTimeNotification(user, notification);
                response.setStatus("OK");
            default:
                response.setStatus("BAD_REQUEST");
                break;
        }

        return response;
    }

    @DeleteMapping("api/notifications/{notificationID}")
    public @ResponseBody Response deleteNotification(@PathVariable String notificationID,
                                                     @RequestParam String userID,
                                                     HttpServletRequest httpServletRequest) {
        Response response = new Response();

//        if (!sessionRepository.isAccessAllowed(userID, httpServletRequest)) {
//            return new Response("NOT_ALLOWED");
//        }

        userRepository.findUserByID(userID).deleteNotification(notificationID);
        response.setStatus("OK");
        return response;
    }

}
