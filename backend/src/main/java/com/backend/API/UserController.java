package com.backend.API;

import com.backend.Repositories.*;
import com.backend.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Array;
import java.util.ArrayList;
import java.util.UUID;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EventRepository events;
    @Autowired
    private ServiceRepository serviceRepository;


    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private ProfessionsRepository professionsRepository;
    @Autowired
    private TokenRepository tokenRepository;

    private PasswordEncoder encoder = new BCryptPasswordEncoder();


    private boolean created = false;

    @PostMapping("/api/users/login") public @ResponseBody Response login(@RequestBody LoginInfo guest,
                                                                         HttpServletResponse httpServletResponse){
        Response response = new Response();
        UserInfo user = userRepository.findUserByLogin(guest.getLogin());

        if(user == null){
            response.setStatus("USER_NOT_FOUND");
        }
        else if (!encoder.matches(guest.getPassword(), user.getPassword())){
            response.setStatus("USER_NOT_FOUND");
        }
        else {
            response.setStatus("OK");
            response.setData(user);
            String cookieValue = UUID.randomUUID().toString();
            httpServletResponse.setHeader("Set-Cookie", cookieValue);
            sessionRepository.deleteSession(user.getUserID());
            sessionRepository.addSession(cookieValue, user.getUserID());
        }
        System.out.println("!!!" + " " + guest.getLogin() + " " + guest.getPassword() + "\n");
        if (!created)
            create();
        return response;
    }

    @PostMapping("api/users/register") public @ResponseBody Response register(@RequestBody UserInfo newer){
        Response response = new Response();

        if(userRepository.findUserByLogin(newer.getLogin()) != null){
            return new Response("USER_EXIST");
        }

        if (!professionsRepository.isExistProfession(newer.getProfession())) {
            response.setStatus("NO_SUCH_PROFESSION");
            return response;
        }

        newer.setPassword(encoder.encode(newer.getPassword()));
        userRepository.addUser(newer);

        response.setStatus("OK");
        response.setData(newer);
        return response;
    }

    @PostMapping("api/users/token") public @ResponseBody Response setToken(@RequestParam String token, HttpServletRequest httpServletRequest) {
        tokenRepository.setToken(sessionRepository.getUserID(httpServletRequest.getHeader("Cookie")), token);

        return new Response("OK");
    }

    @GetMapping("/api/users/{id}") public @ResponseBody Response getUserInfo(@PathVariable String id, HttpServletRequest httpServletRequest) {
        Response response = new Response();

//        if (!sessionRepository.isAccessAllowed(id, httpServletRequest)) {
//            return new Response("NOT_ALLOWED");
//        }

        UserInfo userById = userRepository.findUserByID(id);
        if(userById != null){
            response.setData(userById);
            response.setStatus("OK");
        }
        else{
            response.setStatus("USER_NOT_FOUND");
        }
        return response;
    }


    @GetMapping("api/users/professions") public @ResponseBody Response getProfessions() {
        Response response = new Response();

        response.setData(professionsRepository.getProfessions());
        response.setStatus("OK");

        return response;
    }

    @PostMapping("api/users/{id}/logout")
    public @ResponseBody Response logout(@PathVariable String id, HttpServletRequest httpServletRequest) {
        Response response = new Response();

//        if (!sessionRepository.isAccessAllowed(id, httpServletRequest)) {
//            return new Response("NOT_ALLOWED");
//        }

        sessionRepository.deleteSession(id);
        tokenRepository.deleteToken(id);
        response.setStatus("OK");

        return response;
    }
    @PostMapping("api/users/change")

    public @ResponseBody Response changeUserInfo(@RequestBody UserInfo changedUser){

        UserInfo user = userRepository.findUserByID(changedUser.getUserID());
        if(user == null){
            return new Response("USER_NOT_FOUND");
        }
        user.setEmail(changedUser.getEmail());
        user.setPhone(changedUser.getPhone());
        user.setFirstName(changedUser.getFirstName());
        user.setSecondName(changedUser.getSecondName());

        return new Response("OK", "user");
    }




    private void create(){

        ArrayList<Service> serv1 = new ArrayList<>();
        TimePeriod period = new TimePeriod("2018-07-15 10:00", "2018-07-15 15:00");
        Service service1 = new Service(period, "Водитель"); //id = 2
        service1.setBusy(true);
        Service service2 = new Service(new TimePeriod("2018-07-15 13:00", "2018-07-15 15:00"),
                "Повар"); //id == 3 (etc)
        serviceRepository.addService(service1);
        serviceRepository.addService(service2);
        serv1.add(service1);
        serv1.add(service2);
        Event event1 = new Event("1", "Знакомство с городом", period, serv1); //id = 2
        event1.setOrganizer(userRepository.findUserByID("1"));
        event1.addParticipant(service1.getServiceID(), userRepository.findUserByID("10"));
        userRepository.findUserByID("10").addBusyDate(service1.getServiceID(), period);
        events.addNewEvent(event1);

//-------------------------------------
        ArrayList<Service> services2 = new ArrayList<>();
        TimePeriod period2 = new TimePeriod("2018-07-15 18:00", "2018-07-16 10:00");
        TimePeriod period2_1 = new TimePeriod("2018-07-15 18:00", "2018-07-16 03:00");
        TimePeriod period2_2 = new TimePeriod("2018-07-15 18:00", "2018-07-15 21:00");
        TimePeriod period2_3 = new TimePeriod("2018-07-16 01:00", "2018-07-16 05:00");
        Service service2_1 = new Service(period2_1, "Бармен");
        Service service2_2 = new Service(period2_2, "Фотограф");
        Service service2_3 = new Service(period2_3, "Водитель");
        serviceRepository.addService(service2_1);
        serviceRepository.addService(service2_2);
        serviceRepository.addService(service2_3);
        services2.add(service2_1);
        services2.add(service2_2);
        services2.add(service2_3);

        Event event2 = new Event("1", "Туса тех, кто выжил в эту сетку", period2, services2);
        event2.setOrganizer(userRepository.findUserByID("1")); //id = 3
        event2.addParticipant(service2_2.getServiceID(), userRepository.findUserByID("4"));
        events.addNewEvent(event2);

        event2.addParticipant(service2_2.getServiceID(), userRepository.findUserByID("4"));
        event2.addParticipant(service2_1.getServiceID(), userRepository.findUserByID("3"));
        userRepository.findUserByID("4").addBusyDate(service2_2.getServiceID(), period2_2);
        userRepository.findUserByID("3").addBusyDate(service2_1.getServiceID(), period2_1);

        userRepository.findUserByID("4").addBusyDate(service2_2.getServiceID(), period2_2);
        userRepository.findUserByID("3").addBusyDate(service2_1.getServiceID(), period2_1);
//-------------------------------------------------------------

        ArrayList<Service> services3 = new ArrayList<>();
        TimePeriod period3 = new TimePeriod("2018-12-26 19:00", "2018-12-26 23:30");
        Service service3_1 = new Service(period3, "Видеограф");
        Service service3_2 = new Service(period3, "Кондитер");
        serviceRepository.addService(service3_1);
        serviceRepository.addService(service3_2);
        services3.add(service3_1);
        services3.add(service3_2);

        Event event3 = new Event("4", "День рождения. ", period3, services3);
        event3.setOrganizer(userRepository.findUserByID("4"));
        events.addNewEvent(event3);
//create event with jobNotification for Tanya

        ArrayList<Service> services4 = new ArrayList<>();
        TimePeriod period4 = new TimePeriod("2018-08-01 19:00", "2018-08-01 23:30");
        Service service4_1 = new Service(period4, "Видеограф");
        Service service4_2 = new Service(period4, "Ведущий");
        serviceRepository.addService(service4_1);
        serviceRepository.addService(service4_2);
        services4.add(service4_1);
        services4.add(service4_2);
        Event event4 = new Event("4", "Пре пре посвяжная пати", period4, services4);
        event4.setOrganizer(userRepository.findUserByID("4"));
        events.addNewEvent(event4);
        userRepository.findUserByID("1").addNotification("1");



        created = true;

    }


}
