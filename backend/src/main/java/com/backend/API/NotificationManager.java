
package com.backend.API;

import com.backend.Repositories.*;
import com.backend.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@org.springframework.stereotype.Service
public class NotificationManager {
    @Autowired
    UserRepository userRepository;
    @Autowired
    NotificationRepository notificationRepository;
    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    EventRepository eventRepository;
    @Autowired TokenRepository tokenRepository;

    public void sendJobNotification(NotificationReceive newNotification,
                                    Service reqService){

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String title = "Job Notification";
        String text = "Job Suggestion";
        headers.set("Authorization", "key=" + tokenRepository.getKEY());


        for (UserInfo user : userRepository.getAllUsers()){
            if(user.getProfession().equals(reqService.getProfession()) &&
                    !user.getUserID().equals(newNotification.getSenderID()) &&
                    !user.isBusy(reqService.getPeriod())){
                user.addNotification(newNotification.getNotificationID());
                String requestJson = "{\"to\":\"" + tokenRepository.getToken(user.getUserID()) + "\", \"notification\": { \"title\":\"" + title + "\", \"body\":\""+text+"\",\"sound\":\"default\"}}";
                HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
                String googleResponse = restTemplate.postForObject("https://fcm.googleapis.com/fcm/send", entity, String.class);
            }
        }
    }

    public ArrayList<NotificationSend> getNotifications(int from, int to,
                                                        UserInfo user){

        ArrayList<NotificationSend> notificationSends = new ArrayList<>();
        for (int i = from; i < to && i < user.getNotificationsID().size(); ++i){
            NotificationReceive notificationReceive =
                    notificationRepository.findNotification(user.getNotificationsID().get(i));
            notificationSends.add(notificationRepository.receiveToSend(notificationReceive));
        }

        return notificationSends;
    }


    public void sendJobAcceptedNotification(UserInfo user, NotificationReceive notification){

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String title = "Job Notification";
        String text = "Job Confirmed";
        headers.set("Authorization", "key=" + tokenRepository.getKEY());


        user.addBusyDate(notification.getServiceID(),
                serviceRepository.findService(notification.getServiceID()).getPeriod());
        eventRepository.findEvent(notification.getEventID()).addParticipant(notification.getServiceID(), user);
        serviceRepository.findService(notification.getServiceID()).setBusy(true);

        NotificationReceive reply = new NotificationReceive("JOB_CONFIRM", user.getUserID(),
                notification.getEventID(),
                notification.getServiceID());

        notificationRepository.addNotification(reply);

        UserInfo org = userRepository.findUserByID(notification.getSenderID());
        org.addNotification(reply.getNotificationID());


        String requestJson = "{\"to\":\"" + tokenRepository.getToken(
                org.getUserID() + "\", \"notification\": { \"title\":\"" + title + "\", \"body\":\""+text+"\",\"sound\":\"default\"}}");
        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
        String googleResponse = restTemplate.postForObject("https://fcm.googleapis.com/fcm/send", entity, String.class);

        user.deleteNotification(notification.getNotificationID());
    }

    public void changeNameNotification(Event oldEvent){
        for(UserInfo user : oldEvent.getParticipantsInfo()){
            String serviceID = oldEvent.findAnyUsersService(user.getUserID());
            if (serviceID == null){
                throw new NullPointerException("ServiceID in changeNameNotification is null");
            }
            NotificationReceive notificationReceive = new NotificationReceive("CHANGED_NAME", oldEvent.getOrgId(),
                    oldEvent.getEventID(), serviceID);
            notificationRepository.addNotification(notificationReceive);
            user.addNotification(notificationReceive.getNotificationID());
        }
    }

    public void changedTimeNotification(String newServiceID, String oldServiceID,
                                        String orgID, String eventID, String userID){

        NotificationReceive newNotification = new NotificationReceive("CHANGED_TIME", orgID,
                eventID, newServiceID, oldServiceID);
        notificationRepository.addNotification(newNotification);

        userRepository.findUserByID(userID).addNotification(newNotification.getNotificationID());
    }

    public void rejectTimeNotification(UserInfo user, NotificationReceive notification){

        //delete old busy date and notification
        user.deleteBusyDate(notification.getOldServiceID(),
                serviceRepository.findService(notification.getOldServiceID()).getPeriod());
        user.deleteNotification(notification.getNotificationID());

        //delete user from Event
        Event event = eventRepository.findEvent(notification.getEventID());
        event.deleteParticipant(notification.getServiceID(), user);

        //service is not busy now
        serviceRepository.findService(notification.getServiceID()).setBusy(false);

        //send to organizer about REJECTION
        NotificationReceive reply = new NotificationReceive("TIME_REJECT", user.getUserID(),
                notification.getEventID(), notification.getServiceID());
        notificationRepository.addNotification(reply);
        userRepository.findUserByID(notification.getSenderID()).addNotification(reply.getNotificationID());
    }

    public void acceptTimeNotification(UserInfo user, NotificationReceive notification){

        user.deleteBusyDate(notification.getOldServiceID(),
                serviceRepository.findService(notification.getOldServiceID()).getPeriod());
        user.addBusyDate(notification.getServiceID(),
                serviceRepository.findService(notification.getServiceID()).getPeriod());

        user.deleteNotification(notification.getNotificationID());
    }
}

