//package com.backend.Interceptors;
//
//import com.backend.Repositories.SessionRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Collections;
//import java.util.List;
//
//public class AuthenticationInterceptor extends HandlerInterceptorAdapter {
//    private final static String LOGIN_REQUEST = "/api/users/login";
//    private final static String REGISTRATION_REQUEST = "/api/users/register";
//    private final static String GET_PROFESSIONS = "/api/users/professions";
//    @Autowired
//    SessionRepository sessionRepository;
//
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
//        String cookie = request.getHeader("Cookie");
//
//        System.out.println("\nInterceptor accepted request\n");
//
//        List<String> list = Collections.list(request.getHeaderNames());
//        for (String headerName : list) {
//            System.out.println("Header Name: " + headerName);
//            String headerValue = request.getHeader(headerName);
//            System.out.println("Header Value: " + headerValue);
//        }
//
//        if (request.getRequestURI().equals(LOGIN_REQUEST) || request.getRequestURI().equals(REGISTRATION_REQUEST) ||
//                                                                    request.getRequestURI().equals(GET_PROFESSIONS)) {
//            return true;
//        }
//        if (!sessionRepository.isThereSession(cookie)) {
//            return false;
//        }
//        sessionRepository.updateSession(cookie);
//
//        return true;
//    }
//}
