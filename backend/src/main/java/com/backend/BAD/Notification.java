//package com.backend.BAD;
//
//import com.backend.models.Event;
//import com.backend.models.Service;
//import com.backend.models.UserInfo;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//
//import java.util.ArrayList;
//
//@JsonIgnoreProperties(value = {"receiversID"})
//public class Notification {
//
//    private String notificationID;
//    private String type;
//
//    private UserInfo sender;
//    private Event event;
//    private Service service;
//    private ArrayList<String> receiversID = new ArrayList<>();
//
//    public Notification(Event event, String type, Service service, UserInfo sender){
//        this.event = event;
//        this.type = type;
//        this.sender = sender;
//        this.service = service;
//    }
//
//    public Notification(){}
//
//    public String getNotificationID() {
//        return notificationID;
//    }
//
//    public void setNotificationID(String notificationID) {
//        this.notificationID = notificationID;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public Service getService() {
//        return service;
//    }
//
//    public void setService(Service service) {
//        this.service = service;
//    }
//
//    public UserInfo getSender() {
//        return sender;
//    }
//
//    public void setSender(UserInfo sender) {
//        this.sender = sender;
//    }
//
//    public Event getEvent() {
//        return event;
//    }
//
//    public void setEvent(Event event) {
//        this.event = event;
//    }
//
//    public ArrayList<String> getReceiversID() {
//        return receiversID;
//    }
//
//    public void setReceiversID(ArrayList<String> receiversID) {
//        this.receiversID = receiversID;
//    }
//
//    public void addReceiver(String receiverID) {
//        receiversID.add(receiverID);
//    }
//}
