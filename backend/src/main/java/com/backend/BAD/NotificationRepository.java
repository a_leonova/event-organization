//package com.backend.BAD;
//
//import com.backend.BAD.Notification;
//import com.backend.models.NotificationReceive;
//import org.springframework.stereotype.Repository;
//
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.ConcurrentMap;
//import java.util.concurrent.atomic.AtomicLong;
//
//@Repository
//public class NotificationRepository {
//
//    private ConcurrentMap<String, Notification> notifications = new ConcurrentHashMap<>();
//    private final AtomicLong sequence = new AtomicLong(1);
//
//    public Notification findNotification(String id){
//        return notifications.get(id);
//    }
//
//    public void addNotification(Notification notification) {
//
//        String id = String.valueOf(sequence.getAndIncrement());
//        notification.setNotificationID(id);
//        notifications.put(id, notification);
//    }
//
//
//    public void deleteNotification(String notificationId) {
//        notifications.remove(notificationId);
//    }
//}
