//package com.backend.BAD;
//
//import com.backend.API.Response;
//import com.backend.Repositories.EventRepository;
//import com.backend.Repositories.UserRepository;
//import com.backend.models.Event;
//import com.backend.models.Service;
//import com.backend.models.UserInfo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//
//@RestController
//public class NotificationController {
//
//    @Autowired
//    NotificationRepository notificationRepository;
//
//    @Autowired
//    EventRepository eventRepository;
//
//    @Autowired
//    UserRepository userRepository;
//
//    @GetMapping("/api/users/{id}/notifications") public @ResponseBody
//    Response getNotifications(@PathVariable String id,
//                              @RequestParam int limit,
//                              @RequestParam int offset) {
//
//        Response response = new Response();
//
//        UserInfo user = userRepository.findUserByID(id);
//        ArrayList<Notification> notificationArrayList = new ArrayList<>();
//
//        for (int i = offset; i < offset + limit && i < user.getNotificationsID().size(); ++i){
//            notificationArrayList.add(notificationRepository.findNotification(user.getNotificationsID().get(i)));
//        }
//        response.setData(notificationArrayList);
//
//
//        if (notificationArrayList.size() == limit){
//            response.setStatus("OK");
//        }
//        else{
//            response.setStatus("END");
//        }
//
//        return response;
//    }
//
//    @PostMapping("/api/notifications/send")
//    public  @ResponseBody Response sendJobNotification(@RequestParam String orgID,
//                                                       @RequestParam int serviceID,
//                                                       @RequestBody Event event) {
//        Service service = event.getRequiredProfessions().get(serviceID);
//        Notification notification = new Notification(event,
//                "JOB_SUGGESTION",
//                service,
//                userRepository.findUserByID(orgID));
//        Response response = new Response();
//
//
//        for (UserInfo user : userRepository.getAllUsers()) {
//            if (user.getProfession().equals(service.getProfession()) && !user.getUserID().equals(orgID) && !user.isBusy(service.getPeriod())) {
//                notification.addReceiver(user.getUserID());
//                notificationRepository.addNotification(notification);
//                user.addNotification(notification.getNotificationID());
//            }
//        }
//        response.setStatus("OK");
//
//        return response;
//    }
//
//    @PostMapping("/api/notifications/{id}") public @ResponseBody Response replyToNotification(@PathVariable String id,
//                                                                                                    @RequestParam String invitationStatus,
//                                                                                                    @RequestParam String userID) {
//        Response response = new Response();
//        Notification notification = notificationRepository.findNotification(id);
//        UserInfo user = userRepository.findUserByID(userID);
//
//        switch (invitationStatus) {
//            case "ACCEPTED":
//                if(notification.getService().isBusy()){
//                    response.setStatus("JOB_BOOKED");
//                    return response;
//                }
//                if(user.isBusy(notification.getService().getPeriod())){
//                    response.setStatus("DATE_BUSY");
//                    return response;
//                }
//                user.addBusyDate(notification.getService().getPeriod());
//                eventRepository.findEvent(notification.getEvent().getEventID()).addParticipant(user);
//
//                //here can be baga!!!!!!!!!!!!
//                notification.getService().setBusy(true);
//                Notification replyNotification = new Notification(notification.getEvent(), "JOB_CONFIRM",
//                        notification.getService(), user);
//                notificationRepository.addNotification(replyNotification);
//                notification.getSender().addNotification(replyNotification.getNotificationID());
//                response.setStatus("OK");
//                break;
//            case "REJECTED":
//                user.deleteNotification(id);
//                response.setStatus("OK");
//                break;
//            default:
//                response.setStatus("BAD_REQUEST");
//                break;
//        }
//
//        return response;
//    }
//
//    @DeleteMapping("api/notifications/{id}}") public @ResponseBody Response deleteNotification(@PathVariable String id,
//                                                                                                 @RequestParam String userId) {
//        Response response = new Response();
//
//        userRepository.getUserById(userId).deleteNotification(id);
//        response.setStatus("OK");
//
//        return response;
//    }
//}
